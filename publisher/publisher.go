package publisher

import (
	"bitbucket.org/testbookdotcom/tpubsub"
	"bitbucket.org/testbookdotcom/tpubsub/models"
	"cloud.google.com/go/pubsub"
	"encoding/json"
	"errors"
	"golang.org/x/net/context"
	"strconv"
	"strings"
	"time"
)

const (
	ATTR_PUBID = "pubId"
	ATTR_ENV   = "env"

	PUBSUB_SCHEDULE_WINDOW = 2 * time.Minute
)

var (
	pubSubClient *pubsub.Client
	env          string
	pubName      string
)

//Ideally should have a lock
var topicMap map[string]*pubsub.Topic

func InitPubSub(pubname, environment string) error {
	env = environment
	pubName = pubname

	var err error
	ctx := context.Background()
	pubSubClient, err = pubsub.NewClient(ctx, tpubsub.PROJECT_ID)
	if err != nil {
		return err
	}
	topicMap = make(map[string]*pubsub.Topic)

	envs := []string{"PRODUCTION", "ALPHA", "DEVELOPMENT"}

	if len(env) != 0 {
		envs = []string{env}
	}

	for _, env := range envs {

		txnTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TRANSACTION_CHECK, []string{env})
		if err != nil {
			return err
		}

		tscTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_STAGE_CHANGE, []string{env})
		if err != nil {
			return err
		}

		rqTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_SUBMITTED, []string{env})
		if err != nil {
			return err
		}

		cuTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_COBRANDING_UPDATED, []string{env})
		if err != nil {
			return err
		}

		ricTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_RE_INIT_CACHE, []string{env})
		if err != nil {
			return err
		}

		notiTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_NOTIFY_ADMIN_FOR_CUT_OFF, []string{env})
		if err != nil {
			return err
		}

		ftscTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_UPDATE_FREE_TEST_DATA_CACHE, []string{env})
		if err != nil {
			return err
		}

		//taaTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_ANALYSIS_CRON, []string{env})
		//if err != nil {
		//	return err
		//}

		allTopics := []string{
			tpubsub.PUBSUB_TOPIC_EMAIL,
			tpubsub.PUBSUB_TOPIC_SMS,
			tpubsub.PUBSUB_TOPIC_SCHEDULE,
			txnTopic,
			tscTopic,
			rqTopic,
			cuTopic,
			ricTopic,
			notiTopic,
			ftscTopic,
			tpubsub.PUBSUB_TOPIC_TEST_ANALYSIS_CRON,
		}

		for _, v := range allTopics {
			topic := pubSubClient.Topic(v)
			exists, err := topic.Exists(ctx)
			if err != nil {
				return err
			}
			if !exists {
				if topic, err = pubSubClient.CreateTopic(ctx, v); err != nil {
					return err
				}
			}
			topicMap[v] = topic
		}
	}

	return nil
}

func PublishFreeTestDataCache(t *models.UpdateFreeTestData, when time.Time) error {
	btsc, err := json.Marshal(t)
	if err != nil {
		return err
	}
	rqTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_UPDATE_FREE_TEST_DATA_CACHE, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(rqTopic, when, btsc, nil)
	return err
}

func PublishTestStageChange(t *models.TestStageChange, when time.Time) error {
	btsc, err := json.Marshal(t)
	if err != nil {
		return err
	}
	rqTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_STAGE_CHANGE, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(rqTopic, when, btsc, nil)
	return err
}

func PublishTestAnalysisCron(t models.TestAnalysisCron, when time.Time) error {
	btsc, err := json.Marshal(t)
	if err != nil {
		return err
	}
	//reqTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_ANALYSIS_CRON, []string{env})
	//if err != nil {
	//	return err
	//}
	_, err = Publish(tpubsub.PUBSUB_TOPIC_TEST_ANALYSIS_CRON, when, btsc, nil)
	return err
}

func PublishTestSubmitted(t *models.TestSubmitted, when time.Time) error {
	bts, err := json.Marshal(t)
	if err != nil {
		return err
	}
	rqTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TEST_SUBMITTED, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(rqTopic, when, bts, nil)
	return err
}

func CheckTransaction(t *models.Transaction, when time.Time) error {
	bts, err := json.Marshal(t)
	if err != nil {
		return err
	}
	txnTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_TRANSACTION_CHECK, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(txnTopic, when, bts, nil)
	return err
}

func ReInitCache(r *models.ReInitCache, when time.Time) error {
	bts, err := json.Marshal(r)
	if err != nil {
		return err
	}
	ricTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_RE_INIT_CACHE, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(ricTopic, when, bts, nil)
	return err
}

func NotifyAdminForExamCutOff(e *models.ExamCutOffTest, when time.Time) error {
	bts, err := json.Marshal(e)
	if err != nil {
		return err
	}
	ricTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_NOTIFY_ADMIN_FOR_CUT_OFF, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(ricTopic, when, bts, nil)
	return err
}

func SendEmail(email *models.Email, when time.Time, priority int) error {
	bts, err := json.Marshal(email)
	if err != nil {
		return err
	}
	_, err = Publish(tpubsub.PUBSUB_TOPIC_EMAIL, when, bts, map[string]string{
		models.EMAIL_ATTR_PRIORITY: strconv.Itoa(priority),
	})
	return err
}

func SendSms(sms *models.SMS, when time.Time, priority int) error {
	bts, err := json.Marshal(sms)
	if err != nil {
		return err
	}
	_, err = Publish(tpubsub.PUBSUB_TOPIC_SMS, when, bts, map[string]string{
		models.SMS_ATTR_PRIORITY: strconv.Itoa(priority),
	})
	return err
}

func StudentUpdated(stu *models.Student) error {
	bts, err := json.Marshal(stu)
	if err != nil {
		return err
	}

	if stu.Username == "" && stu.Mobile == "" && stu.Dp == "" && stu.Email == "" && stu.Name == "" && stu.CreatedOn == 0 {
		return errors.New("Either all empty values passed or a new field added in model")
	}

	txnTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_STUDENT_UPDATED, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(txnTopic, time.Now(), bts, nil)
	return err
}

func CobrandingUpdated(c *models.CobrandingInfo) error {
	bts, err := json.Marshal(c)
	if err != nil {
		return err
	}

	if !c.CenterId.Valid() {
		return errors.New("No center id !")
	}

	txnTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_COBRANDING_UPDATED, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(txnTopic, time.Now(), bts, nil)
	return err
}

func StudentCreated(stu *models.Student) error {
	bts, err := json.Marshal(stu)
	if err != nil {
		return err
	}

	if stu.Id.Hex() == "" || stu.CreatedOn == 0 {
		return errors.New("Id and createdOn cant be empty")
	}

	txnTopic, err := tpubsub.ParseKey(tpubsub.PUBSUB_TOPIC_STUDENT_CREATED, []string{env})
	if err != nil {
		return err
	}
	_, err = Publish(txnTopic, time.Now(), bts, nil)
	return err
}

func Publish(topic string, when time.Time, data []byte, attr map[string]string) (*pubsub.PublishResult, error) {
	ctx := context.Background()
	now := time.Now()

	if strings.Contains(topic, tpubsub.KEY_VAR_PLACEHOLDER) {
		return nil, errors.New("Placeholder found in topic")
	}

	if attr == nil {
		attr = make(map[string]string)
	}
	attr[ATTR_PUBID] = pubName
	attr[ATTR_ENV] = env

	var msg pubsub.Message
	var t *pubsub.Topic
	var ok bool
	if now.Add(PUBSUB_SCHEDULE_WINDOW).After(when) {
		t, ok = topicMap[topic]
		if !ok {
			return nil, errors.New(topic + " topic not found !")
		}

		msg = pubsub.Message{
			Data:       data,
			Attributes: attr,
		}
	} else {
		t, ok = topicMap[tpubsub.PUBSUB_TOPIC_SCHEDULE]
		if !ok {
			return nil, errors.New("Schedule topic not found !")
		}
		bts, err := json.Marshal(models.SchedulerPayload{
			Topic: topic,
			Data:  data,
			When:  when,
			Attr:  attr,
		})
		if err != nil {
			return nil, err
		}
		msg = pubsub.Message{
			Data: bts,
		}
	}
	return t.Publish(ctx, &msg), nil
}
