#!/bin/bash
ENV_FILE=env.txt
ENV_FILE_SAMPLE=env.txt_sample
GCE_CACHE_FILE=gce_cache.json
SUPERVISOR_FILE=run_supervisor.conf
SUPERVISOR_FILE_SAMPLE=run_supervisor.conf_sample
RUN_FILE=run.sh
DEPLOY_FILE=deploy.sh
CLOUD_FILES=($ENV_FILE_SAMPLE $GCE_CACHE_FILE $RUN_FILE $DEPLOY_FILE $SUPERVISOR_FILE_SAMPLE)
PACKAGES=(schedulerWorker)
BUCKET_LOCATION=tb_configs/tpubsub
PARENT_PACKAGE=bitbucket.org/testbookdotcom/tpubsub

##Note:-
##Use only the func name for func call

createBinaries(){
    source $HOME/.bashrc
    gvm use go1.11.1
    for pkg in ${PACKAGES[@]}
    do
        echo "Starting with $pkg"
        rm -f temp
        go build -o temp $PARENT_PACKAGE/$pkg
        mv temp bin_$pkg
        if [ -e bin_$pkg ]
        then
            echo "bin_$pkg binary created"
        else
            echo "Failed to create bin_$pkg"
        fi
    done
}

uploadCloudFilesToGCS(){
    for cldfile in ${CLOUD_FILES[@]}
    do
        echo "Uploading $cldfile to GCS"
        if [ -f $cldfile ]; then
            gsutil cp $cldfile gs://$BUCKET_LOCATION/$cldfile
        else
            echo "$cldfile file not found"
            exit
        fi
    done
}

uploadBinariesToGCS(){
    for pkg in ${PACKAGES[@]}
    do
        echo "Uploading bin_$pkg to GCS"
        if [ -f bin_$pkg ]; then
            gsutil cp bin_$pkg gs://$BUCKET_LOCATION/bin_$pkg
        else
            echo "bin_$pkg file not found"
            exit
        fi
    done
}

downloadCloudFilesFromGCS(){
    for cldfile in ${CLOUD_FILES[@]}
    do
        echo "Downloading Cloud file $cldfile"
        gsutil cp gs://$BUCKET_LOCATION/$cldfile $cldfile
    done
}

downloadBinariesFromGCS(){
    for pkg in ${PACKAGES[@]}
    do
        echo "Downloading Binary $pkg"
        gsutil cp gs://$BUCKET_LOCATION/bin_$pkg bin_$pkg
    done
}

deleteBinaries(){
    echo "Removing temp file"
    rm -f temp
    for pkg in ${PACKAGES[@]}
    do
        echo "Removing binary bin_$pkg"
        rm -f bin_$pkg
    done
}

makeExecBinaries(){
    for pkg in ${PACKAGES[@]}
    do
        echo "Making binary bin_$pkg executable"
        chmod +x "bin_$pkg"
    done
}

updateSupervisor(){
    supervisorctl reread
    supervisorctl update
    supervisorctl reload
}

echo -n "Enter role [general/master/slave]: "
read ROLE

#################Master Mode#################
if [ "$ROLE" = "master" ] || [ "$ROLE" = "m" ]; then
    source $HOME/.bashrc
    gvm use go1.11.1
    deleteBinaries
    createBinaries
    uploadCloudFilesToGCS
    uploadBinariesToGCS

###############Slave Mode####################
elif [ "$ROLE" = "slave" ] || [ "$ROLE" = "s" ]; then
    downloadCloudFilesFromGCS
    downloadBinariesFromGCS
    cp $ENV_FILE_SAMPLE $ENV_FILE
    echo GOOGLE_APPLICATION_CREDENTIALS=\"$PWD/$GCE_CACHE_FILE\" >> $ENV_FILE
    makeExecBinaries
    chmod +x $RUN_FILE
    sed 's@__ENV_PWD__@'$PWD'@g' $SUPERVISOR_FILE_SAMPLE > $SUPERVISOR_FILE
    updateSupervisor

################General Mode##################
elif [ "$ROLE" = "general" ] || [ "$ROLE" = "g" ]; then
    source $HOME/.bashrc
    gvm use go1.11.1
    deleteBinaries
    createBinaries
    makeExecBinaries
    cp $ENV_FILE_SAMPLE $ENV_FILE
    echo GOOGLE_APPLICATION_CREDENTIALS=\"$PWD/$GCE_CACHE_FILE\" >> $ENV_FILE
    chmod +x $RUN_FILE
    sed 's@__ENV_PWD__@'$PWD'@g' $SUPERVISOR_FILE_SAMPLE > $SUPERVISOR_FILE
    updateSupervisor

else
    echo "Unknown value entered"
    exit
fi
echo "Completed successfully"
