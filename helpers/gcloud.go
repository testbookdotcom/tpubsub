package helpers

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"cloud.google.com/go/storage"
)

func DownloadFromGCS(cloudLocation, localLocation string) (string, error) {
	pathArray := strings.Split(cloudLocation, "/")
	bucket := pathArray[3]
	fileName := pathArray[len(pathArray)-1]
	cloudLocation = strings.Join(pathArray[4:], "/")
	ctx := context.Background()

	client, err := storage.NewClient(ctx)
	if err != nil {
		return "", err
	}

	rc, err := client.Bucket(bucket).Object(cloudLocation).NewReader(ctx)
	if err != nil {
		return "", err
	}

	defer rc.Close()

	location, err := filepath.Abs(localLocation + "/" + fileName)
	if err != nil {
		return "", err
	}

	file, err := os.Create(location)
	if err != nil {
		return "", err
	}
	defer file.Close()

	_, err = io.Copy(file, rc)
	if err != nil {
		return "", err
	}

	err = client.Bucket(bucket).Object(cloudLocation).Delete(ctx)
	if err != nil {
		return "", err
	}

	return location, nil
}

func main() {
	_, err := DownloadFromGCS("profile.jpg", "profile.jpg")
	if err != nil {
		fmt.Println(err.Error())
	}
	return
}
