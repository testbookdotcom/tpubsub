package helpers

import (
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func Download(url string, localLocation string) (string, error) {
	slugs := strings.Split(url, "/")
	if slugs[2] == "storage.googleapis.com" {
		return DownloadFromGCS(url, localLocation)
	} else {
		return DownloadFile(url, localLocation)
	}
}

func DownloadFile(url string, localLocation string) (string, error) {

	// Create the file
	slugs := strings.Split(url, "/")
	fileName := slugs[len(slugs)-1]
	location, err := filepath.Abs(localLocation + "/" + fileName)
	if err != nil {
		return "", err
	}
	out, err := os.Create(location)
	if err != nil {
		return "", err
	}
	defer out.Close()

	// Get the data
	if strings.HasPrefix(url, "//") {
		url = "https:" + url
	}
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", errors.New("Bad status code: " + strconv.Itoa(resp.StatusCode) + " " + err.Error())
		}
		err = errors.New("Bad status code: " + strconv.Itoa(resp.StatusCode) + " " + string(body))
		return "", err
	}

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return "", err
	}

	return location, nil
}
