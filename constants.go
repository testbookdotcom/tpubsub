package tpubsub

import (
	"errors"
	"strings"
)

const (
	PROJECT_ID          = "testbook-app"
	KEY_VAR_PLACEHOLDER = "?"

	PUBSUB_TOPIC_EMAIL              = "email1"
	PUBSUB_TOPIC_SMS                = "sms"
	PUBSUB_TOPIC_SCHEDULE           = "scheduler"
	PUBSUB_TOPIC_TEST_ANALYSIS_CRON = "cronjob_action_testanalysis"

	PUBSUB_TOPIC_TRANSACTION_CHECK           = KEY_VAR_PLACEHOLDER + ".trans_check"
	PUBSUB_TOPIC_TEST_SUBMITTED              = KEY_VAR_PLACEHOLDER + ".test_submitted"
	PUBSUB_TOPIC_TEST_STAGE_CHANGE           = KEY_VAR_PLACEHOLDER + ".test_stage_change"
	PUBSUB_TOPIC_STUDENT_UPDATED             = KEY_VAR_PLACEHOLDER + ".stu_updated"
	PUBSUB_TOPIC_STUDENT_CREATED             = KEY_VAR_PLACEHOLDER + ".stu_created"
	PUBSUB_TOPIC_RE_INIT_CACHE               = KEY_VAR_PLACEHOLDER + ".re_init_cache"
	PUBSUB_TOPIC_NOTIFY_ADMIN_FOR_CUT_OFF    = KEY_VAR_PLACEHOLDER + ".notify_admin_for_cut_off"
	PUBSUB_TOPIC_UPDATE_FREE_TEST_DATA_CACHE = KEY_VAR_PLACEHOLDER + ".free_test_data_cache"

	//published from ocrm
	PUBSUB_TOPIC_COBRANDING_UPDATED = KEY_VAR_PLACEHOLDER + ".cobranding_updated"

	DOWNLOAD_PATH  = "Downloads/"
	ANALYTICS_PATH = "analytics/"
	INVOICE_PATH   = "ocrm/invoice/"
)

func ParseKey(key string, vars []string) (string, error) {
	arr := strings.Split(key, KEY_VAR_PLACEHOLDER)
	actualKey := ""
	if len(arr) != len(vars)+1 {
		return "", errors.New("pubsub/models: Insufficient arguments to parse key")
	} else {
		for index, val := range arr {
			if index == 0 {
				actualKey = arr[index]
			} else {
				actualKey += vars[index-1] + val
			}
		}
	}
	return actualKey, nil
}
