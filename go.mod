module bitbucket.org/testbookdotcom/tpubsub

require (
	cloud.google.com/go v0.35.1
	github.com/boltdb/bolt v1.3.1
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a
	golang.org/x/sys v0.0.0-20181122145206-62eef0e2fa9b // indirect
	google.golang.org/api v0.1.0
	google.golang.org/grpc v1.17.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
