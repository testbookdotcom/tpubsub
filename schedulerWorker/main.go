package main

import (
	"log"
	"os"
)

func main() {

	gceCred := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")

	if gceCred == "" {
		log.Fatalln("Incomplete environment", gceCred)
	}

	err := Start()
	if err != nil {
		log.Fatalln(err)
	}

}
