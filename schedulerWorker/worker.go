package main

import (
	"bitbucket.org/testbookdotcom/tpubsub"
	"bitbucket.org/testbookdotcom/tpubsub/models"
	"bitbucket.org/testbookdotcom/tpubsub/publisher"
	"cloud.google.com/go/pubsub"
	"encoding/json"
	"golang.org/x/net/context"
	"log"
	"time"
)

const (
	subName = "tb-sub-scheduler"
)

var checkFrequency = time.Minute

func Start() error {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, tpubsub.PROJECT_ID)
	if err != nil {
		return err
	}

	// create topic if it does not exist
	topicName := tpubsub.PUBSUB_TOPIC_SCHEDULE
	topic := client.Topic(topicName)
	exists, err := topic.Exists(ctx)
	if err != nil {
		return err
	}
	if !exists {
		if _, err := client.CreateTopic(ctx, topicName); err != nil {
			return err
		}
	}

	// create subscription if it does not exist
	sub := client.Subscription(subName)
	exists, err = sub.Exists(ctx)
	if err != nil {
		return err
	}
	if !exists {
		if _, err := client.CreateSubscription(ctx, subName, pubsub.SubscriptionConfig{Topic: topic, AckDeadline: 300 * time.Second}); err != nil {
			return err
		}
	}

	db, err := getBoltdbStore()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = publisher.InitPubSub("scheduler", "")
	if err != nil {
		log.Fatalln(err)
	}
	go startPublisher(db)

	return sub.Receive(ctx, func(ctx context.Context, m *pubsub.Message) {
		if err := saveInDatastore(m.Data, db); err != nil {
			log.Println(err)
			return
		}
		m.Ack()
	})
}

func saveInDatastore(bts []byte, db DataStore) error {
	var payload models.SchedulerPayload
	if err := json.Unmarshal(bts, &payload); err != nil {
		return err
	}

	return db.Set(payload.Topic, payload.When, bts)
}

func startPublisher(db DataStore) error {
	for {
		earliestTime, _ := publishOld(db)
		earliest := earliestTime.Sub(time.Now())
		if !earliestTime.IsZero() && earliest < checkFrequency {
			time.Sleep(earliest)
		} else {
			time.Sleep(checkFrequency)
		}
	}
}

func publishOld(db DataStore) (time.Time, error) {
	keys, allBts, err := db.GetBefore(time.Now())
	if err != nil {
		return time.Time{}, err
	}
	for i, v := range allBts {
		//fmt.Println(time.Now(), "got from db")
		var payload models.SchedulerPayload
		if err := json.Unmarshal(v, &payload); err != nil {
			log.Println("Error in publish:", err, v)
			continue
		}

		_, err = publisher.Publish(payload.Topic, time.Now(), payload.Data, payload.Attr)
		if err != nil {
			log.Println("Error in publish:", err, v)
			continue
		}
		err = db.Delete(keys[i])
		if err != nil {
			log.Println("Error in publish:", err, v)
			continue
		}
	}

	return db.GetLatest(checkFrequency)
}
