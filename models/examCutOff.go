package models

import "github.com/globalsign/mgo/bson"

type ExamCutOffTest struct {
	Id         bson.ObjectId `json:"id"`
	IsLiveTest bool          `json:"isLiveTest"`
}

type CutOffEmail struct {
	Id             bson.ObjectId  `json:"id"`
	Title          string         `json:"title"`
	CutOffAnalysis CutOffAnalysis `json:"cutOffAnalysis"`
	CutOffs        ExamCutOffs    `json:"cutOffs"`
}

type ExamCutOffs struct {
	OverAllCutOffs   SectionalCutOff   `json:"overAll,omitempty"`
	SectionalCutOffs []SectionalCutOff `json:"sectional,omitempty"`
}
type SectionalCutOff struct {
	SerialNumber int          `json:"SNo"`
	Title        string       `json:"title"`
	CutOffs      []CutOffElem `json:"cutOffs"`
}
type CutOffElem struct {
	Category   string   `json:"category"`
	LowerBound *float64 `json:"lowerBound,omitempty"`
	UpperBound *float64 `json:"upperBound,omitempty"`
}

type CutOffAnalysis struct {
	OverAll   CutOffSectionalAnalysis   `json:"overAll"`
	Sectional []CutOffSectionalAnalysis `json:"sectional"`
}
type CutOffSectionalAnalysis struct {
	Title            string               `json:"title"`
	Average          float64              `json:"average"`
	Highest          float64              `json:"highest"`
	TopPoint1Average float64              `json:"topPoint1Average"`
	Percentiles      []PercentileMarkElem `json:"percentiles"`
}
type PercentileMarkElem struct {
	Percentile float64 `json:"percentile"`
	Marks      float64 `json:"marks"`
}
