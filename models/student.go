package models

import (
	"github.com/globalsign/mgo/bson"
)

type Student struct {
	Id        bson.ObjectId `json:"_id"`
	Name      string        `json:"name"`
	Email     string        `json:"email"`
	Mobile    string        `json:"mobile"`
	Dp        string        `json:"dp"`
	Username  string        `json:"username"`
	CreatedOn int64         `json:"createdOn"` //unix timestamp
}
