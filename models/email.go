package models

const (
	default_sender_name  = "Testbook Admin"
	default_sender_email = "admin@testbook.com"

	EMAIL_ATTR_PRIORITY = "priority"
)

type Email struct {
	To          MailingList
	Cc          MailingList
	Bcc         MailingList
	From        MailingContact
	Subject     string
	Body        string
	ReplyTo     string
	Attachments []string
}
type MailingContact struct {
	Address string
	Name    string
}

type MailingList []MailingContact

func (m MailingList) GetEmails() []string {
	emails := []string{}
	for _, v := range m {
		emails = append(emails, v.Address)
	}
	return emails
}

func (m MailingList) getNames() []string {
	names := []string{}
	for _, v := range m {
		names = append(names, v.Name)
	}
	return names
}
func (m MailingList) GetLength() int {
	return len(m)
}
func (m MailingList) Append(c MailingContact) MailingList {
	return append(m, c)
}

func GetMailingContact(to_email string, to_name string) MailingContact {
	return MailingContact{Address: to_email, Name: to_name}
}
func GetMailingList(to_email string, to_name string) MailingList {
	return MailingList([]MailingContact{
		GetMailingContact(to_email, to_name),
	})
}
func GetBackEndDevelopers() MailingList {
	return MailingList([]MailingContact{
		GetMailingContact("tb-backend@testbook.com", "Testboook Backend"),
	})
}
func GetDefaultMailingList() MailingList {
	return MailingList([]MailingContact{
		GetMailingContact(default_sender_email, default_sender_name),
	})
}

func GetBranchReporters() MailingList {
	return MailingList([]MailingContact{
		GetMailingContact("partners@testbook.com", "Testbook Partners"),
		GetMailingContact("manish.mishra@testbook.com", "Manish Mishra"),
	})
}

func GetVideoStreamMailingList() MailingList {
	return MailingList([]MailingContact{
		GetMailingContact("satish.pandey@testbook.com", "satish.pandey@testbook.com"),
		GetMailingContact("alu.ranjan@testbook.com", "alu.ranjan@testbook.com"),
		GetMailingContact("niranjan.alu@testbook.com", "niranjan.alu@testbook.com"),
		GetMailingContact("rounak.thombare@testbook.com", "rounak.thombare@testbook.com"),
		GetMailingContact("abhishek.rathi@testbook.com", "abhishek.rathi@testbook.com"),
		GetMailingContact("keshav@testbook.com", "keshav@testbook.com"),
		GetMailingContact("somya.parasar@testbook.com", "somya.parasar@testbook.com"),
		GetMailingContact("mansi.anand@testbook.com", "mansi.anand@testbook.com"),
		GetMailingContact("lakshay.choudhary@testbook.com", "lakshay.choudhary@testbook.com"),
		GetMailingContact("nikhil.singh@testbook.com", "nikhil.singh@testbook.com"),
	})
}