package models

const (
	SMS_ATTR_PRIORITY = "priority"
	PRIORITY_HIGH   = 1000
	PRIORITY_NORMAL = 100
	PRIORITY_LOW    = 10
)

type SMS struct {
	Mobile  string
	Message string
}
