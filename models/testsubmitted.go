package models

import "github.com/globalsign/mgo/bson"

type TestSubmitted struct {
	TestId bson.ObjectId
	Sid    bson.ObjectId
}
