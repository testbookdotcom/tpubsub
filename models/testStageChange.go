package models

import "github.com/globalsign/mgo/bson"

type TestStageChange struct {
	TestId bson.ObjectId
	From   string
	To     string
}
