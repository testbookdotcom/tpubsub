package models

import "github.com/globalsign/mgo/bson"

type CobrandingInfo struct {
	//If sid is empty then all students will be updated
	Sid       bson.ObjectId `bson:"sid" json:"sid"`
	CenterId  bson.ObjectId `bson:"centerId" json:"centerId"`
	Logo      string        `bson:"logo" json:"logo"`
	ValidTill int64         `bson:"validTill" json:"validTill"` //unix timestamp
}
